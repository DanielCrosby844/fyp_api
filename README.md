# Final Year Project - Application Programming Interface

The http server for my final year project built using Java 11 and gradle.

## Other Repositories

- UI: https://bitbucket.org/DanielCrosby844/fyp_ui
- Build: https://bitbucket.org/DanielCrosby844/fyp_build

## Setup

The server requires a mongo database instance to connect to. It also requires a config.json file in the app directory of the project.

```javascript
// config.json
{
  "port": 8080, // The port the server should start on
  "debug": false, // Debug mode flag
  "mongoConnectionString": "mongodb://localhost:27017", // URL to the mongo database
}
```

## Run

```console
gradlew run
```

