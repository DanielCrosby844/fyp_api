package db;

import java.util.ArrayList;

import com.mongodb.client.MongoCursor;

import org.bson.Document;
import org.bson.types.ObjectId;

import data.Account;

public final class AccountsCollection extends Collection {

    public AccountsCollection() {
        super("accounts");
    }

    public ArrayList<String> readUsernames() {
        ArrayList<String> documents = new ArrayList<>();
        MongoCursor<Document> cursor = collection.find().iterator();
        while (cursor.hasNext()) {
            Document document = cursor.next();
            String username = (String) document.get("username");
            String id = ((ObjectId) document.get("_id")).toString();
            documents.add("{ \"username\": \"" + username + "\", \"id\": \"" + id + "\" }");
        }
        return documents;
    }

    public final boolean isValidAccount(final Account account) {
        return collection.find(account.mapToDocument()).first() != null;
    }

    public final boolean isValidUsername(final Account account) {
        return collection.find(new Document("username", account.getUserName())).first() != null;
    }

}
