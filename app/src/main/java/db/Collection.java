package db;

import java.util.ArrayList;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import org.bson.Document;
import org.bson.types.ObjectId;

import data.DataObject;

public class Collection {

    protected final MongoCollection<Document> collection;

    public Collection(final String name) {
        this.collection = MongoDB.getCollection(name);
    }

    public final void create(final DataObject data) {
        collection.insertOne(data.mapToDocument());
    }

    public final ArrayList<String> read() {
        ArrayList<String> documents = new ArrayList<>();
        MongoCursor<Document> cursor = collection.find().iterator();
        while (cursor.hasNext()) {
            documents.add(cursor.next().toJson());
        }
        return documents;
    }

    public final void update(final DataObject data) {
        Document query = new Document("_id", new ObjectId(data.getID()));
        collection.updateOne(query, new Document("$set", data.mapToDocument()));
    }

    public final void delete(final DataObject data) {
        ObjectId id = new ObjectId(data.getID());
        Document query = new Document("_id", id);
        Document document = collection.find(query).first();
        MongoDB.deleteFile(document.getString("fileName"));
        collection.deleteOne(query);
    }

}
