package db;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.model.GridFSFile;

import org.bson.Document;

import com.mongodb.client.MongoClient;

import data.Account;
import data.FileRange;
import util.LSSLogger;

public final class MongoDB {

    private final static Logger LOGGER = LSSLogger.getLogger();

    private static MongoDatabase mongoDB;
    private static GridFSBucket gridFSBucket;

    public static final void connect(final String mongoConnectionString) {
        try {
            LOGGER.info("Attempting to connect to MongoDB service...");
            MongoClient mongoClient = MongoClients.create(mongoConnectionString);
            mongoDB = mongoClient.getDatabase("LSS");
            Account admin = new Account("admin", "admin");
            AccountsCollection accountsCollection = new AccountsCollection();
            boolean adminExists = !accountsCollection.isValidUsername(admin);
            LOGGER.info("Connection established");
            if (adminExists) {
                LOGGER.fine("Admin account not found, creating new admin account");
                accountsCollection.create(admin);
            } else {
                LOGGER.fine("Admin account ok");
            }
            gridFSBucket = GridFSBuckets.create(mongoDB);
        } catch (Exception e) {
            LOGGER.severe("Failed to connect, system exiting");
            System.exit(1);
        }
    }

    public static final MongoCollection<Document> getCollection(String collectionName) {
        return mongoDB.getCollection(collectionName);
    }

    public static final void importFileSync(final File file) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(file);
        gridFSBucket.uploadFromStream(file.getName(), fileInputStream);
        fileInputStream.close();
        file.delete();
    }

    public static final void importFile(final File file) {
        new Thread(() -> {
            try {
                MongoDB.importFileSync(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public static final FileRange getFileRange(final String fileName, final long start, final long end)
            throws IOException {
        GridFSDownloadStream downloadStream = gridFSBucket.openDownloadStream(fileName);
        downloadStream.skip(start);
        long fileLength = downloadStream.getGridFSFile().getLength();
        ArrayList<Byte> fileBytes = new ArrayList<>();
        for (int i = 0; i < end; i++) {
            if ((start + i) >= fileLength) {
                break;
            }
            fileBytes.add((byte) downloadStream.read());
        }
        byte[] bodyBytes = new byte[fileBytes.size()];
        for (int i = 0; i < fileBytes.size(); i++) {
            bodyBytes[i] = fileBytes.get(i).byteValue();
        }
        return new FileRange(fileLength, bodyBytes);
    }

    public static final boolean doesFileExist(String fileName) {
        MongoCursor<GridFSFile> cursor = gridFSBucket.find().cursor();
        while (cursor.hasNext()) {
            if (fileName.equals(cursor.next().getFilename())) {
                return true;
            }
        }
        return false;
    }

    public static final void downloadFile(String fileName, OutputStream downloadStream) {
        gridFSBucket.downloadToStream(fileName, downloadStream);
    }

    public static final void deleteFile(String fileName) {
        if (fileName == null || fileName.equals("")) {
            return;
        }
        MongoCursor<GridFSFile> cursor = gridFSBucket.find().cursor();
        while (cursor.hasNext()) {
            GridFSFile gfsFile = cursor.next();
            if (fileName.equals(gfsFile.getFilename())) {
                gridFSBucket.delete(gfsFile.getId());
                return;
            }
        }
    }

}
