package util;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public final class LSSLogger {

    private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private static ConsoleHandler handler;
    private static final ArrayList<String> logBuffer = new ArrayList<>();

    static {
        Logger.getLogger("org.mongodb.driver").setLevel(Level.SEVERE);
        LOGGER.setUseParentHandlers(false);
        handler = new ConsoleHandler();
        Formatter formatter = new Formatter() {
            @Override
            public String format(LogRecord record) {
                LocalTime currentTime = LocalTime.now();
                LocalDate currentDate = LocalDate.now();
                String log = "[" + currentDate + ", " + currentTime.getHour() + ":" + currentTime.getMinute() + ":"
                        + currentTime.getSecond() + "]" + "[" + record.getLevel() + "]: " + record.getMessage() + "\n";
                logBuffer.add(log);
                return log;
            }
        };
        handler.setFormatter(formatter);
        LOGGER.addHandler(handler);
        LocalTime currentTime = LocalTime.now();
        LocalDate currentDate = LocalDate.now();
        try {
            File f = new File("./logs/LLS-log-" + currentDate + "_" + currentTime.getHour() + "-"
                    + currentTime.getMinute() + "-" + currentTime.getSecond() + ".txt");
            if (!f.exists()) {
                File parent = f.getParentFile();
                if (!parent.exists()) {
                    parent.mkdirs();
                }
                f.createNewFile();
            }
            FileHandler fileHandler = new FileHandler(f.getAbsolutePath());
            fileHandler.setFormatter(formatter);
            LOGGER.addHandler(fileHandler);
        } catch (Exception e) {
            LOGGER.warning("Failed to start log pipeline: " + e.getMessage());
        }
    }

    public static final void setLevel(final Level level) {
        LOGGER.setLevel(level);
        handler.setLevel(level);
    }

    public static final Logger getLogger() {
        return LOGGER;
    }

    public static final void welcomeMessage(final int port) throws UnknownHostException {
        LOGGER.info("");
        LOGGER.info("/**        ********  ********");
        LOGGER.info("/**       **//////  **////// ");
        LOGGER.info("/**      /**       /**       ");
        LOGGER.info("/**      /*********/*********");
        LOGGER.info("/**      ////////**////////**");
        LOGGER.info("/**             /**       /**");
        LOGGER.info("/******** ********  ******** ");
        LOGGER.info("//////// ////////  ////////  ");
        LOGGER.info("");
        LOGGER.info("Application available at: http://localhost:" + port + "/app");
        LOGGER.info("On your network: http://" + InetAddress.getLocalHost().getHostAddress() + ":" + port + "/app");
        LOGGER.info("");
    }

}
