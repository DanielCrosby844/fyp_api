package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class Config {

    private final static Logger LOGGER = LSSLogger.getLogger();

    private final JsonObject jsonObject;

    public Config(final JsonObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public final String getProperty(final String key) {
        return jsonObject.get(key).getAsString();
    }

    public static final Config load() {
        try {
            FileInputStream fis = new FileInputStream(new File("./config.json"));
            String json = new String(fis.readAllBytes());
            JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);
            fis.close();
            return new Config(jsonObject);
        } catch (IOException e) {
            LOGGER.severe("Failed to load config file, system exiting");
            System.exit(1);
            return null;
        }
    }

}
