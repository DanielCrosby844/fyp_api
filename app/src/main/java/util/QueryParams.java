package util;

import java.util.HashMap;

import com.sun.net.httpserver.HttpExchange;

public class QueryParams {

    public static final HashMap<String, String> parse(final HttpExchange request) {
        String[] queryStrings = request.getRequestURI().toString().split("\\?")[1].split("&");
        HashMap<String, String> queryParams = new HashMap<>();
        for (String query : queryStrings) {
            String[] querySplit = query.split("=");
            queryParams.put(querySplit[0], querySplit[1]);
        }
        return queryParams;
    }

}
