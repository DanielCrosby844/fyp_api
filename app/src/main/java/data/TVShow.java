package data;

import org.bson.Document;

public class TVShow extends DataObject {

    private String title;
    private String description;
    private String imageURL;

    public TVShow(final String title, final String description, final String imageURL) {
        this.title = title;
        this.description = description;
        this.imageURL = imageURL;
    }

    public final String getTitle() {
        return this.title;
    }

    public final String getDescription() {
        return this.description;
    }

    public final String getImageURL() {
        return this.imageURL;
    }

    @Override
    public Document mapToDocument() {
        return new Document("title", title)
            .append("description", description.replaceAll("[^\\x00-\\x7F]", ""))
            .append("imageURL", imageURL);
    }

}
