package data;

import org.bson.Document;

public final class Account extends DataObject {

    private String username;
    private String password;

    public Account(final String username, final String password) {
        this.username = username;
        this.password = password;
    }

    public final String getUserName() {
        return this.username;

    }

    public final String getPassword() {
        return this.password;
    }

    @Override
    public Document mapToDocument() {
        return new Document("username", username)
            .append("password", password);
    }

}
