package data;

import org.bson.Document;

public final class Movie extends DataObject {

    private String title;
    private String description;
    private String imageURL;
    private String fileName;

    public Movie(final String title, final String description, final String imageURL, final String fileName) {
        this.title = title;
        this.description = description;
        this.imageURL = imageURL;
        this.fileName = fileName;
    }

    public final String getTitle() {
        return this.title;
    }

    public final String getDescription() {
        return this.description;
    }

    public final String getImageURL() {
        return this.imageURL;
    }

    public final String getFileName() {
        return this.fileName;
    }

    @Override
    public Document mapToDocument() {
        return new Document("title", title)
            .append("description", description.replaceAll("[^\\x00-\\x7F]", ""))
            .append("imageURL", imageURL)
            .append("fileName", fileName);
    }

}
