package data;

import org.bson.Document;

public final class Image extends DataObject {

    private String fileName;

    public Image(final String fileName) {
        this.fileName = fileName;
    }

    public final String getFileName() {
        return this.fileName;
    }

    @Override
    public Document mapToDocument() {
        return new Document("fileName", fileName);
    }

}
