package data;

import org.bson.Document;

public abstract class DataObject {

    private String id;

    public String getID() {
        return this.id;
    }

    public abstract Document mapToDocument();

}
