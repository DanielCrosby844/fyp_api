package data;

import org.bson.Document;

public final class Music extends DataObject {

    private String title;
    private String artist;
    private String fileName;

    public Music(final String title, final String artist, final String fileName) {
        this.title = title;
        this.artist = artist;
        this.fileName = fileName;
    }

    public final String getTitle() {
        return this.title;
    }

    public final String getArtist() {
        return this.artist;
    }

    public final String getFileName() {
        return this.fileName;
    }

    @Override
    public Document mapToDocument() {
        return new Document("title", title)
            .append("title", title)
            .append("artist", artist)
            .append("fileName", fileName);
    }

}
