package data;

public final class FileRange {

    private final long fileLength;
    private final byte[] data;

    public FileRange(final long fileLength, final byte[] data) {
        this.fileLength = fileLength;
        this.data = data;
    }

    public long getFileLength() {
        return this.fileLength;
    }

    public byte[] getData() {
        return this.data;
    }

    public int getDataSize() {
        return this.data.length;
    }

}
