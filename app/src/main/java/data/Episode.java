package data;

import org.bson.Document;

public class Episode extends DataObject {

    private String tvshow;
    private int seasonNumber;
    private String title;
    private String description;
    private int episodeNumber;
    private String fileName;

    public Episode(final String tvshow, final int seasonNumber, final String title, final String description,
            final int episodeNumber, final String fileName) {
        this.tvshow = tvshow;
        this.seasonNumber = seasonNumber;
        this.title = title;
        this.description = description;
        this.episodeNumber = episodeNumber;
        this.fileName = fileName;
    }

    public String getTVShow() {
        return this.tvshow;
    }

    public int getSeasonNumber() {
        return this.seasonNumber;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }

    public int getEpisodeNumber() {
        return this.episodeNumber;
    }

    public String getFileName() {
        return this.fileName;
    }

    @Override
    public Document mapToDocument() {
        return new Document("tvshow", tvshow)
            .append("seasonNumber", seasonNumber)
            .append("title", title)
            .append("description", description.replaceAll("[^\\x00-\\x7F]", ""))
            .append("episodeNumber", episodeNumber)
            .append("fileName", fileName);
    }

}
