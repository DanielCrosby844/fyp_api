package data;

import org.bson.Document;

public class Season extends DataObject {

    private String tvshow;
    private String title;
    private String description;
    private int seasonNumber;
    private String imageURL;

    public Season(final String tvshow, final String title, final String description, final int seasonNumber,
            final String imageURL) {
        this.tvshow = tvshow;
        this.title = title;
        this.description = description;
        this.seasonNumber = seasonNumber;
        this.imageURL = imageURL;
    }

    public final String getTVShow() {
        return this.tvshow;
    }

    public final String getTitle() {
        return this.title;
    }

    public final String getDescription() {
        return this.description;
    }

    public final int getSeasonNumber() {
        return this.seasonNumber;
    }

    public final String getImageURL() {
        return this.imageURL;
    }

    @Override
    public Document mapToDocument() {
        return new Document("tvshow", tvshow)
            .append("title", title)
            .append("description", description.replaceAll("[^\\x00-\\x7F]", ""))
            .append("imageURL", imageURL)
            .append("seasonNumber", seasonNumber);
    }

}
