package security;

import java.util.HashMap;
import java.util.List;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.sun.net.httpserver.HttpExchange;

import data.Account;
import db.AccountsCollection;
import util.QueryParams;

public final class Auth {

    private static final Algorithm ALGORITHM = Algorithm.HMAC256("secret");
    private static final JWTVerifier VERIFIER = JWT.require(ALGORITHM).withIssuer("LSS").build();

    public static final String createToken(String name) {
        return JWT.create().withClaim("owner", name).withIssuer("LSS").sign(ALGORITHM);
    }

    public static final boolean isValidToken(final HttpExchange request) {
        try {
            List<String> authList = request.getRequestHeaders().get("Authorization");
            String token;
            if (authList == null) {
                HashMap<String, String> queryParams = QueryParams.parse(request);
                token = queryParams.get("Authorization");
                token = token.split("%20")[1];
            } else {
                token = authList.get(0);
                token = token.split(" ")[1];
            }
            DecodedJWT jwt = VERIFIER.verify(token);
            Claim claim = jwt.getClaims().get("owner");
            return new AccountsCollection().isValidUsername(new Account(claim.asString(), null));
        } catch (Exception e) {
            return false;
        }
    }

}
