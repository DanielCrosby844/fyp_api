import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import db.MongoDB;
import routes.*;
import util.Config;
import util.LSSLogger;

import com.sun.net.httpserver.HttpServer;

public final class Main {

    private final static Logger LOGGER = LSSLogger.getLogger();

    public static final void main(final String[] args) {
        try {
            Config config = Config.load();
            LSSLogger.setLevel(Boolean.parseBoolean(config.getProperty("debug")) ? Level.ALL : Level.INFO);
            MongoDB.connect(config.getProperty("mongoConnectionString"));
            int port = Integer.parseInt(config.getProperty("port"));
            HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
            server.setExecutor(Executors.newCachedThreadPool());
            Route[] routes = { new AccountsRoute(), new FileRoute(), new StreamRoute(), new LoginRoute(),
                    new MoviesRoute(), new MusicRoute(), new ImagesRoute(), new TVShowsRoute(), new SeasonsRoute(),
                    new EpisodesRoute(), new AppRoute() };
            for (Route route : routes) {
                server.createContext(route.getContext(), route);
            }
            LOGGER.fine("Starting server...");
            server.start();
            LOGGER.info("Server started on port " + port);
            LSSLogger.welcomeMessage(port);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
