package routes;

import java.io.IOException;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import data.Episode;
import db.Collection;
import util.LSSLogger;

public final class EpisodesRoute extends SecureRoute {

    private static final Logger LOGGER = LSSLogger.getLogger();
    private static final Collection COLLECTION = new Collection("episodes");

    @Override
    protected void get(final HttpExchange request) throws IOException {
        LOGGER.fine("Retriving all episode data");
        String json = COLLECTION.read().toString();
        request.getResponseHeaders().add("Content-Type", "application/json");
        request.sendResponseHeaders(200, json.length());
        request.getResponseBody().write(json.getBytes());
        request.close();
    }

    @Override
    protected void post(final HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Episode episode = new Gson().fromJson(json, Episode.class);
        COLLECTION.create(episode);
        LOGGER.info("Episode created: " + episode.getTitle());
        request.sendResponseHeaders(201, 0);
        request.close();
    }

    @Override
    protected void put(final HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Episode episode = new Gson().fromJson(json, Episode.class);
        COLLECTION.update(episode);
        LOGGER.info("Episode updated: " + episode.getTitle());
        request.sendResponseHeaders(204, -1);
        request.close();
    }

    @Override
    protected void delete(final HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Episode episode = new Gson().fromJson(json, Episode.class);
        COLLECTION.delete(episode);
        LOGGER.info("Episode deleted by id: " + episode.getID());
        request.sendResponseHeaders(204, -1);
        request.close();
    }

    @Override
    public String getContext() {
        return "/tvshows/episodes";
    }

}
