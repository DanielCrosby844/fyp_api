package routes;

import java.io.IOException;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import data.Music;
import db.Collection;
import util.LSSLogger;

public final class MusicRoute extends SecureRoute {

    private static final Logger LOGGER = LSSLogger.getLogger();
    private static final Collection COLLECTION = new Collection("music");

    @Override
    protected void get(final HttpExchange request) throws IOException {
        LOGGER.fine("Retriving all music data");
        String json = COLLECTION.read().toString();
        request.getResponseHeaders().add("Content-Type", "application/json");
        request.sendResponseHeaders(200, json.length());
        request.getResponseBody().write(json.getBytes());
        request.close();
    }

    @Override
    protected void post(final HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Music music = new Gson().fromJson(json, Music.class);
        COLLECTION.create(music);
        LOGGER.info("Music created: " + music.getTitle());
        request.sendResponseHeaders(201, 0);
        request.close();
    }

    @Override
    protected void put(final HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Music music = new Gson().fromJson(json, Music.class);
        COLLECTION.update(music);
        LOGGER.info("Music updated: " + music.getTitle());
        request.sendResponseHeaders(204, -1);
        request.close();
    }

    @Override
    protected void delete(final HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Music music = new Gson().fromJson(json, Music.class);
        COLLECTION.delete(music);
        LOGGER.info("Music deleted by id: " + music.getID());
        request.sendResponseHeaders(204, -1);
        request.close();
    }

    @Override
    public String getContext() {
        return "/music";
    }

}
