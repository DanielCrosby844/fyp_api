package routes;

import java.io.IOException;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import data.TVShow;
import db.Collection;
import util.LSSLogger;

public final class TVShowsRoute extends SecureRoute {

    private static final Logger LOGGER = LSSLogger.getLogger();
    private static final Collection COLLECTION = new Collection("tvshows");

    @Override
    protected void get(final HttpExchange request) throws IOException {
        LOGGER.fine("Retriving all tvshow data");
        String json = COLLECTION.read().toString();
        request.getResponseHeaders().add("Content-Type", "application/json");
        request.sendResponseHeaders(200, json.length());
        request.getResponseBody().write(json.getBytes());
        request.close();
    }

    @Override
    protected void post(final HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        TVShow tvshow = new Gson().fromJson(json, TVShow.class);
        COLLECTION.create(tvshow);
        LOGGER.info("TVShow created: " + tvshow.getTitle());
        request.sendResponseHeaders(201, 0);
        request.close();
    }

    @Override
    protected void put(final HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        TVShow tvshow = new Gson().fromJson(json, TVShow.class);
        COLLECTION.update(tvshow);
        LOGGER.info("TVShow updated: " + tvshow.getTitle());
        request.sendResponseHeaders(204, -1);
        request.close();
    }

    @Override
    protected void delete(final HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        TVShow tvshow = new Gson().fromJson(json, TVShow.class);
        COLLECTION.delete(tvshow);
        LOGGER.info("TVShow deleted by id: " + tvshow.getID());
        request.sendResponseHeaders(204, -1);
        request.close();
    }

    @Override
    public String getContext() {
        return "/tvshows";
    }

}
