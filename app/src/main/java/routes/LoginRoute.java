package routes;

import java.io.IOException;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import data.Account;
import db.AccountsCollection;
import security.Auth;
import util.LSSLogger;

public final class LoginRoute extends Route {

    private static final Logger LOGGER = LSSLogger.getLogger();
    private static final AccountsCollection COLLECTION = new AccountsCollection();

    @Override
    protected void post(final HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Account account = new Gson().fromJson(json, Account.class);
        if (!COLLECTION.isValidAccount(account)) {
            LOGGER.warning("Login attempt failed for user : " + account.getUserName());
            request.sendResponseHeaders(401, 0);
            return;
        }
        String token = Auth.createToken(account.getUserName());
        LOGGER.info("Login Token created for user: " + account.getUserName());
        request.getResponseHeaders().add("Authorization", "Bearer " + token);
        request.sendResponseHeaders(200, 0);
        request.close();
    }

    @Override
    public String getContext() {
        return "/accounts/login";
    }

}
