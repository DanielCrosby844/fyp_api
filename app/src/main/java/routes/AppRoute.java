package routes;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.sun.net.httpserver.HttpExchange;

public final class AppRoute extends Route {

    @Override
    protected void get(final HttpExchange request) throws IOException {
        File file = new File("." + request.getRequestURI().toString());
        if (file.exists() && !file.isDirectory()) {
            FileInputStream in = new FileInputStream(file);
            byte[] bytes = in.readAllBytes();
            request.sendResponseHeaders(200, bytes.length);
            request.getResponseBody().write(bytes);
            in.close();
        } else {
            file = new File("./app/index.html");
            if (file.exists() && !file.isDirectory()) {
                FileInputStream in = new FileInputStream(file);
                byte[] bytes = in.readAllBytes();
                request.sendResponseHeaders(200, bytes.length);
                request.getResponseBody().write(bytes);
                in.close();
            }
            request.sendResponseHeaders(404, 0);
        }
        request.close();

    }

    @Override
    public String getContext() {
        return "/app";
    }

}
