package routes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import data.Account;
import db.AccountsCollection;
import util.LSSLogger;

public final class AccountsRoute extends SecureRoute {

    private static final Logger LOGGER = LSSLogger.getLogger();
    private static final AccountsCollection COLLECTION = new AccountsCollection();

    @Override
    protected void get(final HttpExchange request) throws IOException {
        LOGGER.fine("Retriving all account names");
        ArrayList<String> accountNames = COLLECTION.readUsernames();
        String json = accountNames.toString();
        request.getResponseHeaders().add("Content-Type", "application/json");
        request.sendResponseHeaders(200, json.length());
        request.getResponseBody().write(json.getBytes());
        request.close();
    }

    @Override
    protected void post(final HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Account account = new Gson().fromJson(json, Account.class);
        COLLECTION.create(account);
        LOGGER.info("Account created for user: " + account.getUserName());
        request.sendResponseHeaders(201, 0);
        request.close();
    }

    @Override
    protected void put(final HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Account account = new Gson().fromJson(json, Account.class);
        COLLECTION.update(account);
        LOGGER.info("Account updated: " + account.getUserName());
        request.sendResponseHeaders(204, -1);
        request.close();
    }

    @Override
    protected void delete(final HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Account account = new Gson().fromJson(json, Account.class);
        COLLECTION.delete(account);
        LOGGER.info("Account deleted by id: " + account.getID());
        request.sendResponseHeaders(204, -1);
        request.close();
    }

    @Override
    public String getContext() {
        return "/accounts";
    }

}
