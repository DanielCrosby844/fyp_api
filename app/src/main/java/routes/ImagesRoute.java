package routes;

import java.io.IOException;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import data.Image;
import db.Collection;
import util.LSSLogger;

public final class ImagesRoute extends SecureRoute {

    private static final Logger LOGGER = LSSLogger.getLogger();
    private static final Collection COLLECTION = new Collection("images");

    @Override
    protected void get(HttpExchange request) throws IOException {
        LOGGER.fine("Retriving all image data");
        String json = COLLECTION.read().toString();
        request.getResponseHeaders().add("Content-Type", "application/json");
        request.sendResponseHeaders(200, json.length());
        request.getResponseBody().write(json.getBytes());
        request.close();
    }

    @Override
    protected void post(HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Image image = new Gson().fromJson(json, Image.class);
        COLLECTION.create(image);
        LOGGER.info("Image created: " + image.getFileName());
        request.sendResponseHeaders(201, 0);
        request.close();

    }

    @Override
    protected void delete(HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Image image = new Gson().fromJson(json, Image.class);
        COLLECTION.delete(image);
        LOGGER.info("Image delete by id: " + image.getID());
        request.sendResponseHeaders(204, -1);
        request.close();
    }

    @Override
    public String getContext() {
        return "/images";
    }

}
