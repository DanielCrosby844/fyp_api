package routes;

import java.io.IOException;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import data.Season;
import db.Collection;
import util.LSSLogger;

public final class SeasonsRoute extends SecureRoute {

    private static final Logger LOGGER = LSSLogger.getLogger();
    private static final Collection COLLECTION = new Collection("seasons");

    @Override
    protected void get(HttpExchange request) throws IOException {
        LOGGER.fine("Retriving all season data");
        String json = COLLECTION.read().toString();
        request.getResponseHeaders().add("Content-Type", "application/json");
        request.sendResponseHeaders(200, json.length());
        request.getResponseBody().write(json.getBytes());
        request.close();
    }

    @Override
    protected void post(HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Season season = new Gson().fromJson(json, Season.class);
        COLLECTION.create(season);
        LOGGER.info("Season created: " + season.getTitle());
        request.sendResponseHeaders(201, 0);
        request.close();
    }

    @Override
    protected void put(HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Season season = new Gson().fromJson(json, Season.class);
        COLLECTION.update(season);
        LOGGER.info("Season updated: " + season.getTitle());
        request.sendResponseHeaders(204, -1);
        request.close();
    }

    @Override
    protected void delete(HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Season season = new Gson().fromJson(json, Season.class);
        COLLECTION.delete(season);
        LOGGER.info("Season deleted by id: " + season.getID());
        request.sendResponseHeaders(204, -1);
        request.close();
    }

    @Override
    public String getContext() {
        return "/tvshows/seasons";
    }

}
