package routes;

import java.io.IOException;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import data.Movie;
import db.Collection;
import util.LSSLogger;

public final class MoviesRoute extends SecureRoute {

    private static final Logger LOGGER = LSSLogger.getLogger();
    private static final Collection COLLECTION = new Collection("movies");

    @Override
    protected void get(final HttpExchange request) throws IOException {
        LOGGER.fine("Retriving all movie data");
        String json = COLLECTION.read().toString();
        request.getResponseHeaders().add("Content-Type", "application/json");
        request.sendResponseHeaders(200, json.length());
        request.getResponseBody().write(json.getBytes());
        request.close();
    }

    @Override
    protected void post(final HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Movie movie = new Gson().fromJson(json, Movie.class);
        COLLECTION.create(movie);
        LOGGER.info("Movie created: " + movie.getTitle());
        request.sendResponseHeaders(201, 0);
        request.close();
    }

    @Override
    protected void put(final HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Movie movie = new Gson().fromJson(json, Movie.class);
        COLLECTION.update(movie);
        LOGGER.info("Movie updated: " + movie.getTitle());
        request.sendResponseHeaders(204, -1);
        request.close();
    }

    @Override
    protected void delete(final HttpExchange request) throws IOException {
        String json = new String(request.getRequestBody().readAllBytes());
        Movie movie = new Gson().fromJson(json, Movie.class);
        COLLECTION.delete(movie);
        LOGGER.info("Movie deleted by id: " + movie.getID());
        request.sendResponseHeaders(204, -1);
        request.close();
    }

    @Override
    public String getContext() {
        return "/movies";
    }

}
