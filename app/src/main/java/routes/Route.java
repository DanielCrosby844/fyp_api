package routes;

import java.io.IOException;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpExchange;

public abstract class Route implements HttpHandler {

    @Override
    public void handle(final HttpExchange request) throws IOException {
        switch (request.getRequestMethod()) {
        case "GET":
            get(request);
            break;
        case "POST":
            post(request);
            break;
        case "PUT":
            put(request);
            break;
        case "DELETE":
            delete(request);
            break;
        default:
            methodNotAllowed(request);
            break;
        }
    }

    protected void get(final HttpExchange request) throws IOException {
        methodNotAllowed(request);
    }

    protected void post(final HttpExchange request) throws IOException {
        methodNotAllowed(request);
    }

    protected void put(final HttpExchange request) throws IOException {
        methodNotAllowed(request);
    }

    protected void delete(final HttpExchange request) throws IOException {
        methodNotAllowed(request);
    }

    public abstract String getContext();

    protected final void methodNotAllowed(final HttpExchange request) throws IOException {
        request.sendResponseHeaders(405, 0);
        request.close();
    }

}
