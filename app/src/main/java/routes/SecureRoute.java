package routes;

import java.io.IOException;
import com.sun.net.httpserver.HttpExchange;

import security.Auth;

public abstract class SecureRoute extends Route {

    @Override
    public void handle(final HttpExchange request) throws IOException {
        if (!Auth.isValidToken(request)) {
            request.sendResponseHeaders(401, 0);
            request.close();
            return;
        }
        super.handle(request);
    }

}
