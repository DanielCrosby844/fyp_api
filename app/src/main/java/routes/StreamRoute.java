package routes;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

import data.FileRange;
import db.MongoDB;
import util.LSSLogger;
import util.QueryParams;

public final class StreamRoute extends SecureRoute {

    private final static Logger LOGGER = LSSLogger.getLogger();

    @Override
    protected void get(final HttpExchange request) throws IOException {
        HashMap<String, String> queryParams = QueryParams.parse(request);
        String fileName = queryParams.get("file");
        if (!MongoDB.doesFileExist(fileName)) {
            request.sendResponseHeaders(404, 0);
        } else {
            Headers requestHeaders = request.getRequestHeaders();
            List<String> range = requestHeaders.get("Range");
            long start = 0;
            if (range != null) {
                Long r = Long.valueOf(range.get(0).split("=")[1].split("-")[0]);
                start = r;
            }
            FileRange fileRange = MongoDB.getFileRange(fileName, start, 1024 * 1024);
            long end = start + fileRange.getDataSize() - 1;
            LOGGER.fine("Streaming file: " + fileName + " Range: " + start + "-" + end);
            Headers responseHeaders = request.getResponseHeaders();
            responseHeaders.add("Content-Range", "bytes " + start + "-" + end + "/" + fileRange.getFileLength());
            responseHeaders.add("Content-Type", "video/mp4");
            responseHeaders.add("Accept-Ranges", "bytes");
            request.sendResponseHeaders(206, 0);
            OutputStream out = request.getResponseBody();
            out.write(fileRange.getData());
        }
        request.close();
    }

    @Override
    public String getContext() {
        return "/file/stream";
    }

}
