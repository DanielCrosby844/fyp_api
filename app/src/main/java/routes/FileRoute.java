package routes;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

import db.MongoDB;
import util.QueryParams;

public final class FileRoute extends SecureRoute {

    @Override
    protected void get(final HttpExchange request) throws IOException {
        HashMap<String, String> queryParams = QueryParams.parse(request);
        String fileName = queryParams.get("file");
        if (!MongoDB.doesFileExist(fileName)) {
            request.sendResponseHeaders(404, 0);
        } else {
            Headers responseHeaders = request.getResponseHeaders();
            responseHeaders.add("Content-Type", "application/octet-stream");
            responseHeaders.add("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            request.sendResponseHeaders(200, 0);
            OutputStream out = request.getResponseBody();
            MongoDB.downloadFile(fileName, out);
        }
        request.close();
    }

    @Override
    protected void post(final HttpExchange request) throws IOException {
        Headers requestHeaders = request.getRequestHeaders();
        String boundary = requestHeaders.get("Content-type").get(0).split("; ")[1].split("=")[1];
        BufferedInputStream bodyStream = new BufferedInputStream(request.getRequestBody());
        int skip = boundary.length() + 4;
        long read = 0;
        byte[] history = new byte[3];
        String multipartData = "";
        while (true) {
            byte current = (byte) bodyStream.read();
            read++;
            if (read > skip) {
                multipartData += (char) current;
                history[0] = history[1];
                history[1] = history[2];
                history[2] = current;
                if (history[0] == '\n' && history[1] == '\r' && history[2] == '\n') {
                    break;
                }
            }
        }
        String fileName = multipartData.split("; ")[2].split("\r\n")[0].split("=")[1];
        File file = new File(fileName.substring(1, fileName.length() - 1));
        Files.copy(bodyStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
        MongoDB.importFileSync(file);
        request.sendResponseHeaders(200, 0);
        request.close();
    }

    @Override
    public String getContext() {
        return "/file";
    }

}
